<?php

use App\Http\Controllers\Api\V1\StudentsController;
use App\Http\Controllers\Api\V1\SubjectsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::namespace('api')->middleware('api')->group(function () {
    Route::namespace('v1')->prefix('v1')->group(function () {
        Route::get('students', [StudentsController::class, 'index'])->name('students');
        Route::get('students/{student}', [StudentsController::class, 'show'])->name('students.show');

        Route::get('subjects', [SubjectsController::class, 'index'])->name('subjects');
        Route::get('subjects/{subject}', [SubjectsController::class, 'show'])->name('subjects.show');
    });
});
