<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'lecture',
        'classroom'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function students(): BelongsToMany
    {
        return $this->belongsToMany(
            Student::class,
            'student_to_subject',
            'subject_id',
            'student_id'
        );
    }
}
