<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Student extends Model
{
    use HasFactory;

    protected $table = 'students';
    protected $fillable = [
        'name',
        'surname',
        'course',
        'birthdate',
        'phone',
        'email'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];


    public function subjects(): BelongsToMany
    {
        return $this->belongsToMany(
            Subject::class,
            'student_to_subject',
            'student_id',
            'subject_id'
        );
    }
}
