<?php

namespace App\Http\Controllers\Api\V1\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentListRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'filter' => 'array',
            'filter.name' => 'string',
            'filter.course' => 'integer|max:5',
            'filter.birthdate<' => 'date',
            'filter.birthdate>' => 'date',
            'filter.lecturer' => 'string',
            'filter.title' => 'string',
            'sort' => 'array',
            'sort.course.*' => Rule::in($this->getAcceptableSortValues()),
            'sort.birthdate.*' => Rule::in($this->getAcceptableSortValues())
        ];
    }

    private function getAcceptableSortValues(): array
    {
        return ['ASC', 'DESC'];
    }

}
