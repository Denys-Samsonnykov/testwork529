<?php

namespace App\Http\Controllers\Api\V1\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubjectListRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'filter' => 'array',
            'filter.lecturer' => 'string',
            'filter.title' => 'string',
            'filter.classroom' => 'integer|min:1|max:635',
            'filter.surname' => 'string',
            'filter.course' => 'integer|min:1|max:5',
            'sort' => 'array',
            'sort.classroom.*' => Rule::in($this->getAcceptableSortValue()),
        ];
    }

    private function getAcceptableSortValue(): array
    {
        return ['ASC', 'DESC'];
    }
}
