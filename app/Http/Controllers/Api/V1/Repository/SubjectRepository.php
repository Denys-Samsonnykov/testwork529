<?php

namespace App\Http\Controllers\Api\V1\Repository;

use App\Models\Subject;
use Illuminate\Database\Eloquent\Collection;

class SubjectRepository
{
    public function getSubjects(array $filters): Collection
    {
        $query = Subject::query();


        if (isset($filters['filter']['title'])) {
            $query = $query->where('title', '=', $filters['filter']['title']);
        }
        if (isset($filters['filter']['lecturer'])) {
            $query = $query->where('lecturer', '=', $filters['filter']['lecturer']);
        }
        if (isset($filters['filter']['classroom'])) {
            $query = $query->where('classroom', '=', $filters['filter']['classroom']);
        }


        $query->whereHas('students', function ($query) use ($filters) {

            if (isset($filters['filter']['surname'])) {
                $query->where('surname', '=', $filters['filter']['surname']);
            }
            if (isset($filters['filter']['course'])) {
                $query->where('course', '=', $filters['filter']['course']);
            }
        });


        if (isset($filters['sort'])) {
            foreach ($filters['sort'] as $field => $value)
                $query->orderBy($field, $value);
        }

        return $query->get();


    }
}
