<?php

namespace App\Http\Controllers\Api\V1\Repository;

use App\Models\Student;
use Illuminate\Database\Eloquent\Collection;

class StudentRepository
{

    public function getStudents(array $filters): Collection
    {
        $query = Student::query();


        if (isset($filters['filter']['name'])) {
            $query = $query->where('name', '=', $filters['filter']['name']);
        }
        if (isset($filters['filter']['course'])) {
            $query = $query->where('course', '=', $filters['filter']['course']);
        }
        if (isset($filters['filter']['birthdate<'])) {
            $query = $query->where('birthdate', '<', $filters['filter']['birthdate<']);
        }
        if (isset($filters['filter']['birthdate>'])) {
            $query = $query->where('birthdate', '>', $filters['filter']['birthdate>']);
        }


        $query->whereHas('subjects', function ($query) use ($filters) {

            if (isset($filters['filter']['lecturer'])) {
                $query->where('lecturer', '=', $filters['filter']['lecturer']);
            }

            if (isset($filters['filter']['title'])) {
                $query->where('title', '=', $filters['filter']['title']);
            }
        });


        if (isset($filters['sort'])) {
            foreach ($filters['sort'] as $field => $value)
                $query->orderBy($field, $value);
        }


        return $query->get();
    }
}
