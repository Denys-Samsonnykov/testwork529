<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\Repository\StudentRepository;
use App\Http\Controllers\Api\V1\Request\StudentListRequest;
use App\Http\Controllers\Controller;
use App\Models\Student;


class StudentsController extends Controller
{
    private StudentRepository $studentRepository;

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }


    public function index(StudentListRequest $request)
    {
        $params = $request->validated();

        $students = $this->studentRepository->getStudents($params);

        return response()->json(['students' => $students]);
    }

    public function show(Student $student)
    {
        return response()->json(['student' => $student]);
    }
}
