<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Api\V1\Repository\SubjectRepository;
use App\Http\Controllers\Api\V1\Request\SubjectListRequest;
use App\Http\Controllers\Controller;
use App\Models\Subject;


class SubjectsController extends Controller
{

    private SubjectRepository $subjectRepository;

    public function __construct(SubjectRepository $subjectRepository)
    {
        $this->subjectRepository = $subjectRepository;
    }


    public function index(SubjectListRequest $request)
    {
        $params = $request->validated();

        $subjects = $this->subjectRepository->getSubjects($params);

        return response()->json(['subjects' => $subjects]);
    }

    public function show(Subject $subject)
    {
        return response()->json(['subject' => $subject]);
    }
}
