<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiAuthenticate
{

    const API_KEY = 'key';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $key = $request->header(self::API_KEY);

        if ($key === null) {
            return response()->json(['error_1' => 'key missing']);
        }
        if ($key !== config('services.api.key')) {
            return response()->json(['error_2' => 'key won\'t validate']);
        }

        return $next($request);
    }
}
