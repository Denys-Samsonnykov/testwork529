<?php

namespace Database\Seeders;

use App\Models\Student;
use App\Models\Subject;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::factory(15)->create();

        foreach (Student::all() as $student){
            $subjects = Subject::all()->random(rand(4,5));
            $student->subjects()->attach($subjects);
        }
    }
}
