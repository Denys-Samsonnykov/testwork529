<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('student_subject')) {
            Schema::create('student_subject', function (Blueprint $table) {
                $table->foreignId('student_id')->constrained();
                $table->foreignId('subject_id')->constrained();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('student_subject')) {
            Schema::dropIfExists('student_subject');
        }
    }
};
